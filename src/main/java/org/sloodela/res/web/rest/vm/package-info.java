/**
 * View Models used by Spring MVC REST controllers.
 */
package org.sloodela.res.web.rest.vm;
